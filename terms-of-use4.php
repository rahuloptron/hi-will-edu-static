<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->

<!-- Mirrored from www.blueowlcreative.com/themes/aquatheme/index3.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Feb 2015 09:50:50 GMT -->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>Terms & Use - AG Digitas</title>
	<meta name="description" content="Terms and Conditions">
	<meta name="author" content="Blueowlcreative.com">

<?php include "includes/css.php" ?>
	
<?php include "includes/js.php" ?>
</head>
<body>
  <div id="wrapper">
  
	<div class="container">
		
		<!-- Main Navigation -->
		<?php include "includes/header.php" ?>
		<!-- Main Navigation::END -->
		
		
	
		<!-- Sequence Slider --> 
		<div class="h15 clear"></div>	
		<script type="text/javascript">	
			var preloader = ($.browser.msie) ? false : true ;
				
			$(document).ready(function(){
				var options = {
					autoPlay: true,
					autoPlayDelay: 5000,
					nextButton: true,
					prevButton: true,
					preloader: preloader,
					animateStartingFrameIn: true,
					transitionThreshold: 500,
					fallback: {
			        	theme: "slide",
			        	speed: 500
			        }
				};
				
				var sequence = $("#sequence").sequence(options).data("sequence");
	
				sequence.afterLoaded = function(){
					$(".info").css('display','block');
					$("#sequence").hover(
					        function() {
					        	$(".prev, .next").stop().animate({opacity:0.7},300);	            
					        },
					        function() {        
					        	$(".prev, .next").stop().animate({opacity:0},300);
					        }
					);
					
					$(".prev, .next").hover(
					        function() {
					        	$(this).stop().animate({opacity:1},200);	            
					        },
					        function() {        
					        	$(this).stop().animate({opacity:0.7},200);
					        }		
					);
				}
			})
		</script>
	
		
		<!-- Sequence Slider::END-->
	
		<!-- Featured Texts Section -->
		<!-- Featured Services Section -->
        <div class="row">

			<div class="sixteen columns">
				<h2 class="title"><span><strong>Term Of use</strong></span></h2>
			
                <p class="big_text4">AG Digitas makes available the information,documents,oftware, and products,various services and subscriptions offered by website desugn subject to terms and conditions accessing this Site, which includes your access to or use of any of the Services, you agree to the Terms of Use.This site will subject you and will give the information of the latest version of Terms of Use posted on this Site </p>	
                
                <h2 class="title"><span><strong>Ownership and Restrictions</strong></span></h2>
			
                <p class="big_text4">AG Digitas has license or has the right to use and provide the Site and Materials on the Site.Indicated, all the Materials featured or displayed on the Site, including text, images, photographs, graphics, illustrations, layout of the Site, sound, software, trade dress, trademarks, patents, and the selection and arrangement
</p>	
          <h2 class="title"><span><strong>Registration Data and Account Security</strong></span></h2>
                <p class="big_text4"><strong>(a)</strong> Provide correct, accurate, current, and complete information about you as prompted by any registration forms on the Site    ("Registration Data")<br>
<strong>(b)</strong> Help maintain accuracy of such data by promptly in a reasonable manner of such changes<br>
<strong>(c)</strong> Maintain the security of your password and login ID<br>
<strong>(d)</strong> Notify Ag Digitas immediately of any unauthorized use of your account or other breach of security<br>
<strong>(e)</strong> Accept all responsibility for any and all activity from your account<br>
<strong>(f)</strong> Accept all risks of unauthorized access to the Registration Data and any other information you provide to Ag Digitas You are responsible for consequences of all uses of your account and identification information whether or not actually or expressly authorized by you.</p>
      
      <h2 class="title"><span><strong>Intellectual Property Rights</strong></span></h2>

                <p class="big_text4">AG Digitas the sole owner or licensee of all the rights to the Site and the Contents except as where indicated otherwise. All Contents are the property of Ag Digitas its affiliates or third parties and are protected by all the applicable laws, including, but not limited to copyright, trademark, trade-names, patents, Internet domain names, data protection, privacy and publicity rights and other similar rights and statutes. All title, ownership and intellectual property rights in the Site and the Content shall remain with Ag Digitas its affiliates or respective owner or licensor, as the case may be and does not pass on to you or your representatives unless specifically agreed to by relevant parties..</p>
                
               <h2 class="title"><span><strong> Copyright</strong></span></h2>
            

                <p class="big_text4">AG DIgitas owns the copyright in the selection, coordination, arrangement and enhancement of the Site. The Site is protected by copyright as a collective work and compilation, pursuant to U.S. copyright laws, international conventions, and other copyright laws as applicable.Ag Digitas respects the intellectual property rights of others, and we expect our user(s) to do the same.</p>
    
    
    
    <h2 class="title"><span><strong> Trademarks</strong></span></h2>
                <p class="big_text4">AG Digitas is the exclusive owner and holder of rademarks, logos or service marks and including any other slogan(s) or design contained in the Site and otherwise used in its business activities its affiliates and subsidiaries, and may not be copied, imitated or used, in whole or in part, without the prior written permission of Ag Digitas. To the extent a Mark or logo does not appear in the aforementioned list or on the Site does not constitute a waiver of any and all intellectual property rights that Ag Digitas or its affiliates and subsidiaries have established in any of its product, feature, or service names or logos.</p>
                
                
           <h2 class="title"><span><strong>     Governing Law and Jurisdiction</strong></span></h2>
                <p class="big_text4">These Terms of Use shall be governed by and construed in accordance with the laws of the State of Texas, without regard to conflicts of laws provisions. The sole and exclusive jurisdiction for any action or proceeding arising out of or related to these Terms of Use shall be an appropriate State or Federal court located in Harris County in the State of Texas and you hereby irrevocably consent to the jurisdiction of such courts.</p>

               <h2 class="title"><span><strong>General Provisions</strong></span></h2>
                <p class="big_text4">If one or more of the provisions contained in these Terms of Use is found to be invalid, illegal or enforceable in any respect, the validity, legality, and enforceability of the remaining provisions shall not be affected. Such provisions shall be revised only to the extent necessary to make them enforceable. These Terms of Use constitute the entire agreement between you and OSSCube with respect to the subject matter hereof, and supersede all previous written or oral agreements between the parties with respect to such subject matter.</p>
                
                
                 <h2 class="title"><span><strong>Your acceptance of these terms</strong></span></h2>

                <p class="big_text4">By using this Site network of web sites, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes. If we decide to change our privacy policy, we will post those changes on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.</p>
                
                <h2 class="title"><span><strong>CONTACTING US</strong></span></h2>
               <p class="big_text4"> If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:<br>
AG DIGITAS<br>
http://www.agdigitas.com<br>
1,Shantaben Estate, Below Leela Banquet Hall,<br> Station Road, Goregaon(E), Mumbai 400063, India<br>
022 6525 7861<br>
                   support@agdigitas.com</p>
       

			</div>
		</div>


















        	

		
			
			
	</div>
	
	
	<!-- Footer -->
	<?php include "includes/footer.php" ?>
	<!-- Footer::END -->

	
  </div>

</body>

<!-- Mirrored from www.blueowlcreative.com/themes/aquatheme/index3.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Feb 2015 09:50:57 GMT -->
</html>