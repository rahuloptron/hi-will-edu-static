<!doctype html>
<html lang="en-gb" class="no-js"> 
<head>
	<title>About Genesis Telesecure</title>
    <meta charset="utf-8" />
	<meta name="keywords" content="" />
	<meta name="description" content="Over 20+ years, we are authorised dealer and suuplier in CCTV Cameras, IP Cameras, EPABX, Access Control, Time Attendance, Fire Alarm, Audio Video and Networking" />
	<link rel="shortcut icon" href="images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />

    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
    <link rel="stylesheet" href="js/accordion/accordion.css" type="text/css" media="all" />
  	<link rel="stylesheet" href="js/progressbar/ui.progress-bar.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<div class="site_wrapper">
 <div class="container_full">
  <?php include "includes/header.php" ?>
</div>
<div class="page_title">
<div class="container">
		<div class="title"><h1>About Us</h1></div>
       </div>
</div>


<div class="clearfix"></div>   
<div class="fullgray_area">
<div class="container">       
        <p>Hi-Will Education is the most experienced and popular Training provider since 10+ years in Rajkot with over 700+ 
        Students. We have an Experienced Faculties with Audio Visual Training Methodology Has Own Machine Shop</p><br/>
        <p>we offering</p>
        
        <div class="clearfix mar_top2"></div>

        <div class="one_half">
    
     <h2> Why @ Hi-Will…?</h2>
  <ul class="accrodation">
      <ul class="list12">
        <li>Experienced Faculties</li>
        <li>Audio Visual Training Methodology</li>
        <li>Has Own Machine Shop</li>
        <li>Closely working with industries</li>
        <li>Skill based training</li>
        <li>Placement assistance</li>
        <li>After placement support</li>
        <li>Free up gradation training</li>
        <li>Money back guarantee</li>
      </ul>
  </ul>
  </div>

        <div class="one_half last">
    
      <h2> Who we are…?</h2>
  <ul class="accrodation">
      <ul class="list12">
        <li>Hi-WiLL Engineering Solution</li> 
        <li>Hi-WiLL Education</li>    
        <li>Hi-TEC Auto parts</li>    
        <li>Sprint mat</li>     
        <li>Tru-Turn Metal machining</li> 
        <li>Impel Motors</li> 
        <li>Impel Transmission</li>
        <li>Impetus Prolific Pvt.Ltd.</li>
      
      </ul>
  </ul>
  </div>


         <div class="clearfix mar_top2"></div>


      <div class="two_third">
      <h2>What we do?</h2> 
      <div class="accrodation">
            <ul class="list12">
        <li>Doing need assessment for manpower in various industries.</li>
        <li>Performing skill gap analysis for various sectors.</li>
        <li>Preparing training program based on these analysis.</li>
        <li>Training to students, engineers and employees of company, industrialists, etc.</li>
        <li>All courses are regularly updates and changes are made as per industrial scenario.</li>
        <li>Currently we are doing skill base training on various subjects like CAD, CAM, Die Designing and CNC Programming.</li>
        <li>The courses are designed as per the industrial requirement.</li>
        <li>Placement of the trainees and after training support.</li>
   
            </ul>      
      </div>

        </div><!-- end about site area -->
         
     <div class="one_third last"> 
      <h2>What do we offer?</h2> 
      <div class="accrodation">
            <ul class="list12">
        <li>Unigraphics NX CAD</li> 
        <li>1Unigraphics NX CAM</li>    
        <li>Delcam Powermill</li>    
        <li>Solid Edge CAD</li>     
        <li>AutoCAD for Mechanical and Civil</li> 
        <li>CNC Programming</li> 
        <li>Die Designing</li>
        <li>Consultancy</li>
        <li>Industrial training on sales, quality and productivity.</li>
            </ul>      
      </div>      
    </div>  
          
     
        </div>
       
    </div>
    
    
  </div>
</div>
 <div class="clearfix mar_bottom4"></div>
<div class="clearfix mar_top4"></div>
<?php include "includes/footer.php" ?>
<link rel="alternate stylesheet" type="text/css" href="css/colors/blue.css" title="blue" />
</div>
<?php include "includes/js.php" ?>
<?php include "includes/ga.php" ?>
 </body>
</html>
