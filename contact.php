<!doctype html>
 <html lang="en-gb" class="no-js"> 
 <head>
	<title>Contact Us - Hi-will Education</title>
	<meta charset="utf-8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link rel="shortcut icon" href="images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css' />	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />

    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<div class="site_wrapper">
 <div class="container_full">
 <?php include "includes/header.php" ?>
</div><!-- end top -->
<div class="clearfix"></div>
<div class="page_title">
<div class="container">
		<div class="title"><h1>Contact</h1></div>
       
	</div>
</div>
<div class="clearfix"></div>   
<div class="container">
	<div class="content_fullwidth">        	
    <div class="one_half">
       <!--   <script type="text/javascript" src="//static.mailerlite.com/data/webforms/412335/i4z4d4.js?v1"></script> -->

       <form class="form" action="/enquiry.php" method="post">
        <label>Enter Contact Name</label>
           <input type="" name="contactName" required>
           <label>Enter Contact Email</label>
           <input type="" name="contactEmail" required>
           <label>Enter Contact Mobile</label>
           <input type="" name="contactMobile" required>
           <label>Please Select Course</label>
           <select name="selectCourse" required>
               <option>Select</option>
               <option>Course 1</option>
               <option>Course 1</option>
               <option>Course 1</option>
               <option>Course 1</option>
               <option>Course 1</option>
               <option>Course 1</option>
           </select>

           <button>Submit</button>
       </form>
<div class="clearfix"></div> 
</div>               
    <div class="one_half last">
    <div class="address-info">
            <h3>Hi Will Education</h3>
                <ul>
                <li>
                307, Rajpath Avenue,<br/> 
                Opp. Aastha Residency, Near Ambedkar Nagar, <br/>
                150 Feet Ring Road, Rajkot.<br/>
                Mobile No - 9825911080<br/>g
                E-mail: hiwill.training@gmail.com <br />    
                </li>
            </ul>
        </div>

         <h3>Find the Address</h3>
            <iframe class="google-map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3692.627442926374!2d70.78691131495388!3d22.254209985347543!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3959ca8ac6f98285%3A0x17929e0f13f77096!2sHiwill+Education!5e0!3m2!1sen!2sin!4v1522987207568" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=WA,+United+States&amp;aq=0&amp;oq=WA&amp;sll=47.605288,-122.329296&amp;sspn=0.008999,0.016544&amp;ie=UTF8&amp;hq=&amp;hnear=Washington,+District+of+Columbia&amp;t=m&amp;z=7&amp;iwloc=A">View Larger Map</a></small>

           
 </div>
</div>
</div><!-- end main content area -->
<div class="clearfix"></div>
<div class="mar_top4"></div>
<div class="clearfix bottom_buildings"></div><!-- end buildings graph -->
</div>
<?php include "includes/footer.php" ?>
<link rel="alternate stylesheet" type="text/css" href="css/colors/blue.css" title="blue" />
</div>
 <?php include "includes/js.php" ?>
<?php include "includes/ga.php" ?>
</body>
</html>
