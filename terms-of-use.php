<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>Genesis Telesecure LLP | Terms and Conditons</title>
	<meta charset="utf-8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.ico" />
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css' />
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    
<!-- just remove the below comments witch color skin you want to use -->
    <!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/lightgreen.css" />-->
    <link rel="stylesheet" href="css/colors/blue.css" />
    <!--<link rel="stylesheet" href="css/colors/green.css" />-->
    <!--<link rel="stylesheet" href="css/colors/red.css" />-->
    <!--<link rel="stylesheet" href="css/colors/cyan.css" />-->
    <!--<link rel="stylesheet" href="css/colors/purple.css" />-->
    <!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
    <!--<link rel="stylesheet" href="css/colors/brown.css" />-->
    
<!-- just remove the below comments witch bg patterns you want to use --> 
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-default.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-one.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-two.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-three.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-four.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-five.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-six.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-seven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eight.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-nine.css" />-->
    
   

    <!-- jquery jcarousel -->
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>


    
<div class="site_wrapper">
	
    <div class="container_full">
    

    <?php include "includes/header.php" ?>

</div><!-- end top -->

<div class="clearfix"></div>
 
<div class="page_title">

	<div class="container">
		<div class="title"><h1>Terms & Conditions</h1></div>
       
	</div>
</div><!-- end page title --> 

<div class="clearfix"></div>   
 

<!-- Contant
======================================= -->

<div class="container">

	<div class="content_fullwidth">
    
  <h2 class="title"><span><strong>Terms Of use</strong></span></h2>
			
                <p class="big_text4">Genesis Telesecure makes available the information, documents, software, and products,various services and subscriptions offered by website desugn subject to terms and conditions accessing this Site, which includes your access to or use of any of the Services, you agree to the Terms of Use.This site will subject you and will give the information of the latest version of Terms of Use posted on this Site </p>	
                
                <div class="clearfix divider_line2"></div>
                
                <h2 class="title"><span><strong>Ownership and Restrictions</strong></span></h2>
			
                <p class="big_text4">Genesis Telesecure has license or has the right to use and provide the Site and Materials on the Site.Indicated, all the Materials featured or displayed on the Site, including text, images, photographs, graphics, illustrations, layout of the Site, sound, software, trade dress, trademarks, patents, and the selection and arrangement
</p>	
         
         
                 <div class="clearfix divider_line2"></div>
          <h2 class="title"><span><strong>Registration Data and Account Security</strong></span></h2>
               <ul class="list3">
				<li>Provide correct, accurate, current, and complete information about you as prompted by any registration forms on the Site    ("Registration Data")</li>
				<li>Help maintain accuracy of such data by promptly in a reasonable manner of such changes</li>
				<li>Maintain the security of your password and login ID</li>
                <li>Notify Genesis Telesecure immediately of any unauthorized use of your account or other breach of security</li>
                <li>Accept all responsibility for any and all activity from your account</li>
                <li>Accept all risks of unauthorized access to the Registration Data and any other information you provide to Genesis Telesecure You are responsible for consequences of all uses of your account and identification information whether or not actually or expressly authorized by you.</li>
               
                
                                                
			</ul> 
     
             <div class="clearfix divider_line2"></div>
      
      <h2 class="title"><span><strong>Intellectual Property Rights</strong></span></h2>

                <p class="big_text4">Genesis Telesecure the sole owner or licensee of all the rights to the Site and the Contents except as where indicated otherwise. All Contents are the property of Genesis Telesecure its affiliates or third parties and are protected by all the applicable laws, including, but not limited to copyright, trademark, trade-names, patents, Internet domain names, data protection, privacy and publicity rights and other similar rights and statutes. All title, ownership and intellectual property rights in the Site and the Content shall remain with Genesis Telesecure its affiliates or respective owner or licensor, as the case may be and does not pass on to you or your representatives unless specifically agreed to by relevant parties..</p>
                
                        <div class="clearfix divider_line2"></div>
                
               <h2 class="title"><span><strong> Copyright</strong></span></h2>
            

                <p class="big_text4">Genesis Telesecure owns the copyright in the selection, coordination, arrangement and enhancement of the Site. The Site is protected by copyright as a collective work and compilation, pursuant to U.S. copyright laws, international conventions, and other copyright laws as applicable.Genesis Telesecure respects the intellectual property rights of others, and we expect our user(s) to do the same.</p>
    
            <div class="clearfix divider_line2"></div>
    
    <h2 class="title"><span><strong> Trademarks</strong></span></h2>
                <p class="big_text4">Genesis Telesecure is the exclusive owner and holder of rademarks, logos or service marks and including any other slogan(s) or design contained in the Site and otherwise used in its business activities its affiliates and subsidiaries, and may not be copied, imitated or used, in whole or in part, without the prior written permission of Genesis Telesecure. To the extent a Mark or logo does not appear in the aforementioned list or on the Site does not constitute a waiver of any and all intellectual property rights that Genesis Telesecure or its affiliates and subsidiaries have established in any of its product, feature, or service names or logos.</p>
                
                
                        <div class="clearfix divider_line2"></div>
                
           <h2 class="title"><span><strong>     Governing Law and Jurisdiction</strong></span></h2>
                <p class="big_text4">These Terms of Use shall be governed by and construed in accordance with the laws of the State of Texas, without regard to conflicts of laws provisions. The sole and exclusive jurisdiction for any action or proceeding arising out of or related to these Terms of Use shall be an appropriate State or Federal court located in Harris County in the State of Texas and you hereby irrevocably consent to the jurisdiction of such courts.</p>
                
                        <div class="clearfix divider_line2"></div>

               <h2 class="title"><span><strong>General Provisions</strong></span></h2>
                <p class="big_text4">If one or more of the provisions contained in these Terms of Use is found to be invalid, illegal or enforceable in any respect, the validity, legality, and enforceability of the remaining provisions shall not be affected. Such provisions shall be revised only to the extent necessary to make them enforceable. These Terms of Use constitute the entire agreement between you and OSSCube with respect to the subject matter hereof, and supersede all previous written or oral agreements between the parties with respect to such subject matter.</p>
                
                
                        <div class="clearfix divider_line2"></div>
                
                 <h2 class="title"><span><strong>Your acceptance of these terms</strong></span></h2>

                <p class="big_text4">By using this Site network of web sites, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes. If we decide to change our privacy policy, we will post those changes on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.</p>
                
                
                        <div class="clearfix divider_line2"></div>
                <h2 class="title"><span><strong>CONTACTING US</strong></span></h2>
               <p class="big_text4"> If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:<br>
Genesis Telesecure<br>
http://www.genesistelesecure.com<br>
1,Shantaben Estate, Below Leela Banquet Hall,<br> Station Road, Goregaon(E), Mumbai 400063, India<br>
022 6525 7861<br>
                   support@agdigitas.com</p>
	
    


    
        
    </div>   

</div>

</div><!-- end main content area -->



<div class="mar_top5"></div>




<!-- Footer
======================================= -->

<?php include "includes/footer.php" ?>


<!-- style switcher -->
<script type="text/javascript" src="js/style-switcher/styleswitcher.js"></script>
<link rel="alternate stylesheet" type="text/css" href="css/colors/lightblue.css" title="lightblue" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/lightgreen.css" title="lightgreen" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/blue.css" title="blue" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/green.css" title="green" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/red.css" title="red" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/cyan.css" title="cyan" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/purple.css" title="purple" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/yellow.css" title="yellow" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/brown.css" title="brown" />


 
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- style switcher -->
<script src="js/style-switcher/jquery-1.js"></script>
<script src="js/style-switcher/styleselector.js"></script>

<!-- main menu -->
<script type="text/javascript" src="js/mainmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/mainmenu/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/mainmenu/selectnav.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript" src="js/jcarousel/jquery.jcarousel.min.js"></script>

<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- top show hide plugin script-->
<script src="js/show-hide-plugin/showHide.js" type="text/javascript"></script>

<!-- scroll up -->
<script type="text/javascript">
    $(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
 
    });
</script>

<!-- jquery jcarousel -->
<script type="text/javascript">

	jQuery(document).ready(function() {
			jQuery('#mycarousel').jcarousel();
	});
	
	jQuery(document).ready(function() {
			jQuery('#mycarouseltwo').jcarousel();
	});
	
	jQuery(document).ready(function() {
			jQuery('#mycarouselthree').jcarousel();
	});
	
	jQuery(document).ready(function() {
			jQuery('#mycarouselfour').jcarousel();
	});
	
</script>

<!-- accordion -->
<script type="text/javascript" src="js/accordion/custom.js"></script>


</body>
</html>
