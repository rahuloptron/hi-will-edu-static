<!doctype html>

 <html lang="en-gb" class="no-js"> 

<head>
	<title>Inquiry - Genesis Telesecure LLP</title>
	
	<meta charset="utf-8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.ico" />
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css' />
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    

    <link rel="stylesheet" href="css/colors/blue.css" />
   
    
   

    <!-- jquery jcarousel -->
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>


    
<div class="site_wrapper">
	
    <div class="container_full">
    

    	
     <?php include "includes/header.php" ?>

</div><!-- end top -->

<div class="clearfix"></div>
 
<div class="page_title">

	<div class="container">
		<div class="title"><h1>Inquiry</h1></div>
	</div>
</div><!-- end page title --> 

<div class="clearfix"></div>   
 

<!-- Contant
======================================= -->

<div class="container">

	<div class="content_fullwidth">
        	
    <div class="one_half">
        
  <script type="text/javascript" src="//static.mailerlite.com/data/webforms/412335/i4z4d4.js?v1"></script>

    <br />
   
    </div>
               
    <div class="one_half last">
    
        <div class="address-info">
            <h3>Address Info</h3>
                <ul>
                <li>
                <strong>GENESIS TELESECURE LLP</strong><br />
                Shop No 8, Gold Coin CHS, <br>
Opp St Anthony's church, Nehru Road, <br>
Vakola, Santacruz East, Mumbai 55<br>
Mobile No - 9867374770<br>
 E-mail: sales@genesistelesecure.com<br>
                </li>
            </ul>
        </div>

         <!--<h3>Find the Address</h3>
            <iframe class="google-map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=WA,+United+States&amp;aq=0&amp;oq=WA&amp;sll=47.605288,-122.329296&amp;sspn=0.008999,0.016544&amp;ie=UTF8&amp;hq=&amp;hnear=Washington,+District+of+Columbia&amp;t=m&amp;z=7&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=WA,+United+States&amp;aq=0&amp;oq=WA&amp;sll=47.605288,-122.329296&amp;sspn=0.008999,0.016544&amp;ie=UTF8&amp;hq=&amp;hnear=Washington,+District+of+Columbia&amp;t=m&amp;z=7&amp;iwloc=A">View Larger Map</a></small>-->
        
    </div>
       </div>
</div><!-- end main content area -->
<div class="mar_top4"></div>
<div class="clearfix bottom_buildings"></div><!-- end buildings graph -->
</div>
<?php include "includes/footer.php" ?>
<link rel="alternate stylesheet" type="text/css" href="css/colors/blue.css" title="blue" />

 
</div>

    
 <?php include "includes/js.php" ?>

<?php include "includes/ga.php" ?>
</body>
</html>
