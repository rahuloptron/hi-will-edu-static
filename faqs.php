<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>ANOVA - Multipurpose Template</title>
	
	<meta charset="utf-8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.ico" />
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css' />
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    
<!-- just remove the below comments witch color skin you want to use -->
    <!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/lightgreen.css" />-->
    <!--<link rel="stylesheet" href="css/colors/blue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/green.css" />-->
    <!--<link rel="stylesheet" href="css/colors/red.css" />-->
    <!--<link rel="stylesheet" href="css/colors/cyan.css" />-->
    <!--<link rel="stylesheet" href="css/colors/purple.css" />-->
    <!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
    <!--<link rel="stylesheet" href="css/colors/brown.css" />-->
    
<!-- just remove the below comments witch bg patterns you want to use --> 
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-default.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-one.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-two.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-three.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-four.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-five.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-six.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-seven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eight.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-nine.css" />-->
    
    <!-- style switcher -->
    <link rel="stylesheet" media="screen" href="js/style-switcher/color-switcher.css" />
    
    <!-- REVOLUTION SLIDER -->
    <link rel="stylesheet" type="text/css" href="js/revolutionslider/css/fullwidth.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="js/revolutionslider/rs-plugin/css/settings.css" media="screen" />
    
    <!-- jquery jcarousel -->
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
	
    <!-- thumbs zoom -->
    <link rel="stylesheet" href="js/thumbszoom/thumbzoom.css" type="text/css" media="all" />
 	
    <!-- faqs -->
    <link rel="stylesheet" href="js/accordion/accordion.css" type="text/css" media="all" />
    
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>

<!-- top show hide --> 
<div class="slidingDiv">
        
    <div class="top_contact_info">
        <div class="container">
        	<ul class="tci_list_left">
            	<li><a href="index.html">Home</a></li>
                <li>|</li>
                <li><a href="#">Help</a></li>
                <li>|</li>
                <li><a href="#">FAQs</a></li>
                <li>|</li>
                <li><a href="#">Buy Now!</a></li>
            </ul>
        
            <ul class="tci_list">
            
                <li class="phone">+88 123 456 7890</li>
                <li class="email"><a href="mailto:info@anova.com">info@anova.com</a></li>
                <li><a href="#"><img src="images/top_si1.png" alt="" /></a></li>
                <li><a href="#"><img src="images/top_si2.png" alt="" /></a></li>
                <li><a href="#"><img src="images/top_si3.png" alt="" /></a></li>
                <li><a href="#"><img src="images/top_si4.png" alt="" /></a></li>
                <li><a href="#"><img src="images/top_si5.png" alt="" /></a></li>
                <li><a href="#"><img src="images/top_si6.png" alt="" /></a></li>
                <li><a href="#"><img src="images/top_si7.png" alt="" /></a></li>
                 
            </ul>
        </div>
    </div><!-- end top contact info -->

</div>
<div class="top-border-line"></div>
<div class="container"><div class="show_hide_butr"><a href="#" class="show_hide"></a></div></div>
<!-- end top show hide --> 
    
<div class="site_wrapper">
	
    <div class="container_full">
    

    	
        <div class="top_section">
    
    	<div class="container">

    		<div class="one_fourth"><div id="logo"><a href="index.html"><h1>ANO<i>V</i>A</h1></a></div></div><!-- end logo -->
            	
            <div class="three_fourth last">
               
               <nav id="access" class="access" role="navigation">
               
                <div id="menu" class="menu">
                    
                    <ul id="tiny">
                    
                        <li><a href="index.html">Home</a>
                        
							<ul>
                            	<li><a href="#" class="arrow"><img src="images/menu-drop-arrow.png" alt="" /></a></li>
                                <li><a href="index-page1.html">Home Page 1</a></li>
                                <li><a href="index-page2.html">Home Page 2</a></li>
                                <li><a href="index-page3.html">Home Page 3</a></li>
                                <li><a href="index-page4.html">Home Page 4</a></li>
                                <li><a href="index-page5.html">Home Page 5</a></li>
                                <li><a href="index-page6.html">Home Page 6</a></li>
                                <li><a href="index-page7.html">Home Page 7</a></li>
                                <li><a href="index-page8.html">Home Page 8</a></li>
                                <li><a href="index-page9.html">Home Page 9</a></li>
                                <li><a href="index-page10.html">Home Page 10</a></li>
							</ul>
                         </li>  
                        
                        <li><a href="#" class="active">Features</a>
                        
                        	<ul>
                            	<li><a href="#" class="arrow"><img src="images/menu-drop-arrow.png" alt="" /></a></li>
                                <li><a href="elements.html">Elements</a></li>
                                <li><a href="typography.html">Typography</a></li>
                                <li><a href="pricing-tables.html">Pricing Tables</a></li>
                                <li><a href="columns.html">Page Columns</a></li>
                                <li><a href="testimonials.html">Testimonials</a></li>
                                <li><a href="faqs.html">FAQs</a></li>
                                <li><a href="tabs.html">Tabs</a></li>
                                <li><a href="#">4 Headers &amp; Footers</a></li>                         
                                <li><a href="#">Custom BGs &amp; Colors</a></li>
                                <li><a href="#">PSD Files Included</a></li>                             
                                <li><a href="#">Clean &amp; Valid Code</a></li>
                                <li><a href="#">Useful Typo Elements</a></li>
                                <li><a href="#">Cross Browser Check</a></li>   
                            </ul>
                            
                        </li>
                        
                        <li><a href="#">Headers</a>
                        
                        	<ul>
                            	<li><a href="#" class="arrow"><img src="images/menu-drop-arrow.png" alt="" /></a></li>
                            	<li><a href="http://gsrthemes.com/anova/layout2/fullwidth/index.html">Header Style 1</a></li>
                                <li><a href="http://gsrthemes.com/anova/layout3/fullwidth/index.html">Header Style 2</a></li>
                                <li><a href="http://gsrthemes.com/anova/layout4/fullwidth/index.html">Header Style 3</a></li>
                                <li><a href="http://gsrthemes.com/anova/layout5/fullwidth/index.html">Header Style 4</a></li>
                                <li><a href="http://gsrthemes.com/anova/layout1/fullwidth/index.html">Header Style 5</a></li>
							</ul>
                        </li>
                        
                        <li><a href="#">Footers</a>
                        
                        	<ul>
                            	<li><a href="#" class="arrow"><img src="images/menu-drop-arrow.png" alt="" /></a></li>
                            	<li><a href="http://gsrthemes.com/anova/layout2/fullwidth/index.html">Footer Style 1</a></li>
                                <li><a href="http://gsrthemes.com/anova/layout3/fullwidth/index.html">Footer Style 2</a></li>
                                <li><a href="http://gsrthemes.com/anova/layout4/fullwidth/index.html">Footer Style 3</a></li>
                                <li><a href="http://gsrthemes.com/anova/layout5/fullwidth/index.html">Footer Style 4</a></li>
                                <li><a href="http://gsrthemes.com/anova/layout1/fullwidth/index.html">Footer Style 5</a></li>
							</ul>
                        </li>
                        
                        <li><a href="#">Sliders</a>
                        
                        	<ul>
                            	<li><a href="#" class="arrow"><img src="images/menu-drop-arrow.png" alt="" /></a></li>
                            	<li><a href="index.html">Revolution Slider</a></li>
                                <li><a href="index-slider-2.html">Nivo Slider</a></li>
                                <li><a href="index-slider-1.html">Thumbs Slider</a></li>
                                <li><a href="index-slider-3.html">Static Image</a></li>
                                <li><a href="index-slider-4.html">Video Slider</a></li>
							</ul>
                        </li>
                          
                        <li><a href="#">Pages</a>
                        
                        	<ul>
                            	<li><a href="#" class="arrow"><img src="images/menu-drop-arrow.png" alt="" /></a></li>
                                <li><a href="about.html">About Page Style 1</a></li>
                                <li><a href="about-2.html">About Page Style 2</a></li>
                                <li><a href="services.html">Services Style 1</a></li>
                                <li><a href="services-2.html">Services Style 2</a></li>
                                <li><a href="full-width.html">Full Width Page</a></li>
                                <li><a href="left-sidebar.html">Left Sidebar Page</a></li>
                                <li><a href="right-sidebar.html">Right Sidebar Page</a></li>
                                <li><a href="left-nav.html">Left Navigation</a></li>
                                <li><a href="right-nav.html">Right Navigation</a></li>
                                <li><a href="404.html">404 Error Page</a></li>
                            </ul>
                        </li>
                        
                        <li><a href="#">Portfolio</a>
                        
                        	<ul>
                            	<li><a href="#" class="arrow"><img src="images/menu-drop-arrow.png" alt="" /></a></li>
                                <li><a href="portfolio-one.html">Single Image</a></li>
                                <li><a href="portfolio-two.html">2 Columns</a></li>
                                <li><a href="portfolio-three.html">3 Columns</a></li>
                                <li><a href="portfolio-four.html">4 Columns</a></li>
                                <li><a href="portfolio-five.html">Portfolio + Sidebar</a></li>
                            </ul>
                        </li>
                        
                        <li><a href="#">Blog</a>
                        
                            <ul>
                                <li><a href="#" class="arrow"><img src="images/menu-drop-arrow.png" alt="" /></a></li>
                                <li><a href="blog.html">with Large Image</a></li>
                                <li><a href="blog-2.html">with Small Image</a></li>
                                <li><a href="blog-post.html">Single Post</a></li>
                            </ul>
                        </li>
                        
                        <li><a href="contact.php">Contact</a></li>
                        
                    </ul>
                    
                </div>
                
          	</nav><!-- end nav menu -->
          
        	</div>
                

		</div> 

	</div>

</div><!-- end top -->

<div class="clearfix"></div>
 
<div class="page_title">

	<div class="container">
		<div class="title"><h1>FAQs</h1></div>
        <div class="pagenation"><a href="index.html">Home</a> <i>/</i> <a href="#">Features</a> <i>/</i> FAQs</div>
	</div>
</div><!-- end page title --> 

<div class="clearfix"></div>   
 

<!-- Contant
======================================= -->

<div class="container">

<div class="content_left">
    
    <div class="accrodation">
    
    	<h2><i>Frequently</i> Asked Questions</h2>
    
    	<!-- section 1 -->
        <span class="acc-trigger active"><a href="#">Responsive HTML5 &amp; CSS3 Theme</a></span>
        <div class="acc-container">
        <div class="content">
        There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration in some form, by injected humour, or randomised words which looks Aenean tempus nunc sitamet nulla aliquet volutpat curabitur semper aliquam lorem eu euismod accumsan  accumsan mollis elementum sem ultrices egestas.
        </div>
        </div>
      
        <!-- section 3 -->
        <span class="acc-trigger"><a href="#">Well Commented + Structured Code</a></span>
        <div class="acc-container">
        <div class="content">
        There are many variations of but the majority have suffered alteration in some form, by injected humour, or randomised words which looks. Lorem Ipsum is simply dummy text of the printing and typesetting indust been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into.  Lorem Ipsum is simply dummy text of the printing and typesetting indust been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into. 
        </div>
        </div>
               
        <!-- section 5 -->
        <span class="acc-trigger"><a href="#">5 Different Awesome Slideshows. Include Slider Revolution worth of $12</a></span>
        <div class="acc-container">
        <div class="content">
        There are many variations of but the majority have suffered alteration in some form, by injected humour, or randomised words which looks.
        </div>
        </div>
        
        <!-- section 6 -->
        <span class="acc-trigger"><a href="#">Custom BG Patterns, Unlimited Colors and Shortcodes</a></span>
        <div class="acc-container">
        <div class="content">
        There are many variations of but the majority have suffered alteration in some form, by injected humour, or randomised words which looks.
        </div>
        </div>
        
        <!-- section 7 -->
        <span class="acc-trigger"><a href="#">10 Different Home page Versions</a></span>
        <div class="acc-container">
        <div class="content">
        There are many variations of but the majority have suffered alteration in some form, by injected humour, or randomised words which looks.
        </div>
        </div>
        
        <!-- section 8 -->
        <span class="acc-trigger"><a href="#">Useful Typography Elements</a></span>
        <div class="acc-container">
        <div class="content">
        There are many variations of but the majority have suffered alteration in some form, by injected humour, or randomised words which looks.
        </div>
        </div>
        
        <!-- section 9 -->
        <span class="acc-trigger"><a href="#">Cross Browser Support and Layered PSD Files</a></span>
        <div class="acc-container">
        <div class="content">
        There are many variations of but the majority have suffered alteration in some form, by injected humour, or randomised words which looks.
        </div>
        </div>
        
        <!-- section 10 -->
        <span class="acc-trigger"><a href="#">Get Much More...</a></span>
        <div class="acc-container">
        <div class="content">
        <p>There are many variations of but the majority have suffered alteration in some form, by injected humour, or randomised words which looks.</p><br />
        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
        </div>
        </div>
    
    </div>
    
</div><!-- end content left side area -->


<!-- right sidebar starts -->
<div class="right_sidebar">

	<div class="sidebar_widget">
    	<div class="sidebar_title"><h3><i>More</i> About Us</h3></div>
		<ul class="arrows_list1">		
            <li><a href="#">Our History</a></li>
            <li><a href="#">Professional Research</a></li>
            <li><a href="#">Our Development</a></li>
            <li><a href="#">Partnership With Us</a></li>
            <li><a href="#">Company Carreer</a></li>
            <li><a href="#">New Technology</a></li>
            <li><a href="#">Client Testimonials</a></li>
		</ul>
	</div><!-- end section -->
    
    <div class="clearfix mar_top3"></div>
    
    <div class="sidebar_widget">
    
    	<div class="sidebar_title"><h3><i>Recent</i> Posts</h3></div>
        
			<ul class="recent_posts_list">
                
                <li>
                <span><a href="#"><img src="images/post-simg1.jpg" alt="" /></a></span>
                <a href="#">Publishing packag esanse web page editos web sites including versions</a>
                <i>June 09, 2013</i> 
                </li>
                
                <li>
                <span><a href="#"><img src="images/post-simg2.jpg" alt="" /></a></span>
                <a href="#">Sublishing packag esanse web page editos web sites</a>
                <i>June 08, 2013</i> 
                </li>
                        
                <li class="last">
                <span><a href="#"><img src="images/post-simg3.jpg" alt="" /></a></span>
                <a href="#">Mublishing packag esanse web page editos including versions</a>
                <i>June 07, 2013</i> 
                </li>

            </ul>
                
	</div><!-- end section -->
    
    <div class="clearfix mar_top3"></div>
    
    <div class="clientsays_widget">
    
    	<div class="sidebar_title"><h3><i>Happy</i> Client Say's</h3></div>
        
        <img src="images/site-img25.jpg" alt="" />
<strong>- Henry Brodie</strong><p>Lorem Ipsum passage, and going through the cites of the word here classical literature passage discovered undou btable source. which looks reasonable of the generated charac eristic words.</p>  
                
	</div><!-- end section -->
    
    <div class="clearfix mar_top8"></div>
    
    <div class="sidebar_widget">
    
    	<div class="sidebar_title"><h3><i>Portfolio</i> Widget</h3></div>
        
  		<div class="portfolio_sidebar_widget">
        
          <ul id="mycarousel" class="jcarousel-skin-tango">
          
            <li>
                <div class="item">
                    <a href="#"><img src="images/site-img2.jpg" alt="" title="" /></a>
                    <div class="caption">
                        <a href="">varius malesuada</a>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                        <i>+</i>
                        </p>
                    </div>
                </div>
            </li>
            
            <li>
            	<div class="item">
                    <a href="#"><img src="images/site-img3.jpg" alt="" title="" /></a>
                    <div class="caption">
                        <a href="">standard text ever</a>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                        <i>+</i>
                        </p>
                    </div>
                </div>
            </li>
            
            <li>
            	<div class="item">
                    <a href="#"><img src="images/site-img4.jpg" alt="" title="" /></a>
                    <div class="caption">
                        <a href="">electro typesetting</a>
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                        <i>+</i>
                        </p>
                    </div>
                </div>
            </li>

          </ul>
          
		</div>
             
	</div><!-- end section -->

	
</div>


</div>


<div class="clearfix"></div>
<div class="mar_top5"></div>

<div class="clearfix bottom_buildings"></div><!-- end buildings graph -->


<!-- Footer
======================================= -->

<div id="footer">

<div class="footer_columns">

	<div class="container">
    
        <div class="one_fourth">
        
        	<h2><i>useful</i> links</h2>
            
            <ul class="arrows_list1">
            <li><a href="#">Home Page Variations</a></li>
            <li><a href="#">Awsome Slidershows</a></li>
            <li><a href="#">Features and Typography</a></li>
            <li><a href="#">Different &amp; Unique Pages</a></li>
            <li><a href="#">Single and Portfolios</a></li>
            <li><a href="#">Recent Blogs or News</a></li>
            <li><a href="#">Layered PSD Files</a></li>
			</ul>
             
        </div><!-- end section --> 
        
        <div class="one_fourth">
        	
            <div class="twitter_feed">
            
            	<h2><i>Latest</i> Tweets</h2>
                
                <div class="left"><img src="images/twitter-bird.png" alt="" /></div>
                <div class="right"><a href="https://twitter.com/gsrthemes9" target="_blank">gsrthemes9</a>: aoxhost - Responsive html5 Professional Hosting Theme<br />
				<a href="#" class="small">.9 days ago</a> .<a href="#" class="small">reply</a> .<a href="#" class="small">retweet</a> .<a href="#" class="small">favorite</a></div>
                
                <div class="clearfix divider_line4"></div>
                
                <div class="left"><img src="images/twitter-bird.png" alt="" /></div>
                <div class="right"><a href="https://twitter.com/gsrthemes9" target="_blank">gsrthemes9</a>: Kinvexy - Responsive HTML5 / CSS3, Simple, Clean and Professional Corporate Theme for Multipurpose Use.<br />
				<a href="#" class="small">.8 days ago</a> .<a href="#" class="small">reply</a> .<a href="#" class="small">retweet</a> .<a href="#" class="small">favorite</a></div>
                
            </div>
        
        </div><!-- end section --> 
        
        <div class="one_fourth">
        
        	<h2><i>Flickr</i> Photos</h2>
            
        	<div id="flickr_badge_wrapper">
            	<script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=9&amp;display=latest&amp;size=s&amp;layout=h&amp;source=user&amp;user=93382411%40N07"></script>     
            </div>
            
        </div><!-- end section --> 
        
        <div class="one_fourth last">
        
        	<h2><i>newsletter</i> signup</h2>
            
        	<div class="newsletter">
            <p>Please provide your email to get latest information of updates special offers, products or events for free.</p>
           
            <form method="get" action="index.html" />   
				<input class="enter_email_input" name="samplees" id="samplees" value="Enter your email address" onfocus="if(this.value == 'Enter your email address') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Enter your email address';}" type="text" />
				<input name="" value="subscribe" class="input_submit" type="submit" />
			</form>
             
             <div class="rss_feeds">Subscribe to the <a href="#">Anova RSS feed</a></div>
             
            </div>
        
        </div><!-- end section -->

</div>
  
</div><!-- end footer all columns --> 

<div class="clearfix divider_line5"></div>

<div class="container">
	<div class="scrollup_area"><a href="#" class="scrollup">i</a></div><!-- end scroll to top of the page-->
</div>

<div class="copyright_info">

    <div class="container">
    
        <div class="three_fourth">
        
            <div class="footer_logo"><a href="index.html"><h1>ANO<i>V</i>A</h1></a></div><!-- end logo -->
            <b>Copyright © 2013 anova.com. All rights reserved.  <a href="#">Terms of Use</a>  <a href="#">Privacy Policy</a></b>
            
        </div>
    
    	<div class="one_fourth last">
     		
            <div class="address_info">
            
            	2901 M P Brook Way, Houston WA 98122<br />
                Phone: +88 123 456 7890<br />
                E-mail: <a href="mailto:gsrthemes9@gmail.com">info@anova.com</a><br />
            
            </div>
                
    	</div>
    
    </div>
    
</div><!-- end copyright info -->  

</div>


<!-- style switcher -->
<script type="text/javascript" src="js/style-switcher/styleswitcher.js"></script>
<link rel="alternate stylesheet" type="text/css" href="css/colors/lightblue.css" title="lightblue" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/lightgreen.css" title="lightgreen" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/blue.css" title="blue" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/green.css" title="green" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/red.css" title="red" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/cyan.css" title="cyan" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/purple.css" title="purple" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/yellow.css" title="yellow" />
<link rel="alternate stylesheet" type="text/css" href="css/colors/brown.css" title="brown" />

<div id="style-selector">
    <div class="style-selector-wrapper">
	<span class="title">Choose Theme Options</span>
	<div>        
<span class="title-sub"></span>

<span class="title-sub2">4 Different Layouts</span>

<ul class="styles-noborder">     
    <li>Layout 1</li>
    <li><a href="http://gsrthemes.com/anova/layout1/fullwidth/index.html"><img src="images/elements/layout-one-full.png" alt="" /></a></li>
    <li><a href="http://gsrthemes.com/anova/layout1/boxed/index.html"><img src="images/elements/layout-one-boxed.png" alt="" /></a></li>
</ul>

<ul class="styles-noborder">     
    <li>Layout 2</li>
    <li><a href="http://gsrthemes.com/anova/layout2/fullwidth/index.html"><img src="images/elements/layout-two-full.png" alt="" /></a></li>
    <li><a href="http://gsrthemes.com/anova/layout2/boxed/index.html"><img src="images/elements/layout-two-boxed.png" alt="" /></a></li>
</ul>

<ul class="styles-noborder">     
    <li>Layout 3</li>
    <li><a href="http://gsrthemes.com/anova/layout3/fullwidth/index.html"><img src="images/elements/layout-three-full.png" alt="" /></a></li>
    <li><a href="http://gsrthemes.com/anova/layout3/boxed/index.html"><img src="images/elements/layout-three-boxed.png" alt="" /></a></li>
</ul>

<ul class="styles-noborder">     
    <li>Layout 4</li>
    <li><a href="http://gsrthemes.com/anova/layout4/fullwidth/index.html"><img src="images/elements/layout-four-full.png" alt="" /></a></li>
    <li><a href="http://gsrthemes.com/anova/layout4/boxed/index.html"><img src="images/elements/layout-four-boxed.png" alt="" /></a></li>
</ul>

<ul class="styles-noborder">     
    <li>Layout 5</li>
    <li><a href="http://gsrthemes.com/anova/layout5/fullwidth/index.html"><img src="images/elements/layout-five-full.png" alt="" /></a></li>
    <li><a href="http://gsrthemes.com/anova/layout5/boxed/index.html"><img src="images/elements/layout-five-boxed.png" alt="" /></a></li>
</ul>

<span class="title-sub2">Predefined Color Skins</span>

<ul class="styles">     
    <li><a href="#" onclick="setActiveStyleSheet('default'); return false;"><span class="pre-color-skin1"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('lightblue'); return false;"><span class="pre-color-skin2"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('lightgreen'); return false;"><span class="pre-color-skin3"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('blue'); return false;"><span class="pre-color-skin4"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('green'); return false;"><span class="pre-color-skin5"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('red'); return false;"><span class="pre-color-skin6"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('cyan'); return false;"><span class="pre-color-skin7"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('purple'); return false;"><span class="pre-color-skin8"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('yellow'); return false;"><span class="pre-color-skin9"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('brown'); return false;"><span class="pre-color-skin10"></span></a></li>
</ul>


<link rel="alternate stylesheet" type="text/css" href="css/bg-patterns/pattern-default.css" title="pattern-default" />
<link rel="alternate stylesheet" type="text/css" href="css/bg-patterns/pattern-one.css" title="pattern-one" />
<link rel="alternate stylesheet" type="text/css" href="css/bg-patterns/pattern-two.css" title="pattern-two" />
<link rel="alternate stylesheet" type="text/css" href="css/bg-patterns/pattern-three.css" title="pattern-three" />
<link rel="alternate stylesheet" type="text/css" href="css/bg-patterns/pattern-four.css" title="pattern-four" />
<link rel="alternate stylesheet" type="text/css" href="css/bg-patterns/pattern-five.css" title="pattern-five" />
<link rel="alternate stylesheet" type="text/css" href="css/bg-patterns/pattern-six.css" title="pattern-six" />
<link rel="alternate stylesheet" type="text/css" href="css/bg-patterns/pattern-seven.css" title="pattern-seven" />
<link rel="alternate stylesheet" type="text/css" href="css/bg-patterns/pattern-eight.css" title="pattern-eight" />
<link rel="alternate stylesheet" type="text/css" href="css/bg-patterns/pattern-nine.css" title="pattern-nine" />

<!-- end Predefined Color Skins --> 

<span class="title-sub2">BG Patterns for Boxed</span>

<ul class="styles" style="border-bottom: none;">     
    <li><a href="#" onclick="setActiveStyleSheet('default'); return false;"><span class="bg-patterns1"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('pattern-one'); return false;"><span class="bg-patterns2"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('pattern-two'); return false;"><span class="bg-patterns3"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('pattern-three'); return false;"><span class="bg-patterns4"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('pattern-four'); return false;"><span class="bg-patterns5"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('pattern-five'); return false;"><span class="bg-patterns6"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('pattern-six'); return false;"><span class="bg-patterns7"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('pattern-seven'); return false;"><span class="bg-patterns8"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('pattern-eight'); return false;"><span class="bg-patterns9"></span></a></li>
    <li><a href="#" onclick="setActiveStyleSheet('pattern-nine'); return false;"><span class="bg-patterns10"></span></a></li>
</ul><!-- end BG Patterns --> 

<a href="#" class="close icon-chevron-right"></a>  
    
</div>
</div>
</div><!-- end style switcher -->
 
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- thumbs zoom -->
<script src="js/thumbszoom/jquery-1.3.1.min.js"></script>
<script src="js/thumbszoom/custom.js"></script>

<!-- style switcher -->
<script src="js/style-switcher/jquery-1.js"></script>
<script src="js/style-switcher/styleselector.js"></script>

<!-- main menu -->
<script type="text/javascript" src="js/mainmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/mainmenu/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/mainmenu/selectnav.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript" src="js/jcarousel/jquery.jcarousel.min.js"></script>

<!-- REVOLUTION SLIDER -->
<script type="text/javascript" src="js/revolutionslider/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- top show hide plugin script-->
<script src="js/show-hide-plugin/showHide.js" type="text/javascript"></script>

<!-- scroll up -->
<script type="text/javascript">
    $(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
 
    });
</script>

<!-- jquery jcarousel -->
<script type="text/javascript">

	jQuery(document).ready(function() {
			jQuery('#mycarousel').jcarousel();
	});
	
	jQuery(document).ready(function() {
			jQuery('#mycarouseltwo').jcarousel();
	});
	
	jQuery(document).ready(function() {
			jQuery('#mycarouselthree').jcarousel();
	});
	
	jQuery(document).ready(function() {
			jQuery('#mycarouselfour').jcarousel();
	});
	
</script>

<!-- accordion -->
<script type="text/javascript" src="js/accordion/custom.js"></script>

<!-- REVOLUTION SLIDER -->
<script type="text/javascript">

		var tpj=jQuery;
		tpj.noConflict();

		tpj(document).ready(function() {

		if (tpj.fn.cssOriginal!=undefined)
			tpj.fn.css = tpj.fn.cssOriginal;

			tpj('.fullwidthbanner').revolution(
				{
					delay:9000,
					startwidth:1000,
					startheight:560,

					onHoverStop:"on",						// Stop Banner Timet at Hover on Slide on/off

					thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
					thumbHeight:50,
					thumbAmount:3,

					hideThumbs:200,
					navigationType:"none",				// bullet, thumb, none
					navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none

					navigationStyle:"round",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


					navigationHAlign:"right",				// Vertical Align top,center,bottom
					navigationVAlign:"bottom",					// Horizontal Align left,center,right
					navigationHOffset:50,
					navigationVOffset:55,

					soloArrowLeftHalign:"left",
					soloArrowLeftValign:"center",
					soloArrowLeftHOffset:0,
					soloArrowLeftVOffset:0,

					soloArrowRightHalign:"right",
					soloArrowRightValign:"center",
					soloArrowRightHOffset:0,
					soloArrowRightVOffset:0,

					touchenabled:"on",						// Enable Swipe Function : on/off



					stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
					stopAfterLoops:0,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic



					fullWidth:"on",

					shadow:0								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

				});




	});
	</script>



</body>
</html>
