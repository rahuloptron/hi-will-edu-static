<!doctype html>
<html lang="en-gb" class="no-js"> 
<head>
	<title>Certificate Course in CAD
with Unigraphics NX &amp;
Plastic Mold Design</title>
	<meta charset="utf-8">
	<meta name="keywords" content="" />
	<meta name="description" content="We are one of the leading Telecom and EPABX service provider in Mumbai since over 20 years.
     Dealing in brands like Panasonic, Samsung, Avaya at best price." />
<?php include "../includes/css-1.php" ?>
</head>
<body>
<div class="wrapper_boxed">
<div class="site_wrapper">
   <div class="container_full"> 
     <?php include "../includes/header.php" ?>
</div>
<div class="clearfix"></div>
 <div class="page_title">
	<div class="container">
		<div class="title"><h1>Certificate Course in CAD with Unigraphics NX &amp; Plastic Mold Design</h1></div>
  
	</div>
</div>
<div class="container">
<div class="content_left">

  <h1>Certificate Course in CAD with Unigraphics NX &amp; Plastic Mold Design</h1>
   <div class="clearfix mar_top2"></div>
  
<p>ACAD LT is ideally suited to amending existing none specialist .dwg files and is widely used for layout planning, architectural drafting, engineering drawings and every day drafting requirements.
</p>
 <div class="clearfix mar_top2"></div>
<p>This hands on course 3/4 day AutoCAD LT training course covers the basic drawing and drafting tools needed for every day use and is an ideal foundation for users new to AutoCad.</p>
<p>Due to the similarity the AutoCAD LT shares with the full version of AutoCAD the skill learnt using this software are directly transferrable to other versions of AutoCAD software should your career path move you in that direction or your company upgrades to one of Autodesk’s more powerful drafting tools.</p>



  <div class="clearfix divider_line2"></div>

<div class="one_half">
   <h2>Content</h2>
     <ul class="fullimage_box">
      <ul class="list5">
      <li>Engineering Drawing</li> 
      <li>NX CAD – Sketcher, Feature Modeling,</li>    
      <li>Synchronous Modeling, Surfacing, Assembly</li>    
      <li>Plastic Mold Design</li>      
      </ul>
      </ul>

</div>

<div class="one_half last">
    <h2>Eligibility</h2>
      <ul class="fullimage_box">
      <ul class="list5">
        <li>Diploma or Degree in Engineering</li> 
        <li>10 th or 12 th Pass</li>    
        <li>Machine Operators</li>    
        <li>Designers and Programmers</li>     
      </ul>
      </ul>

</div>

   <div class="clearfix mar_top2"></div>      

<div class="blog_post">  
  <div class="blog_postcontent">
  <div class="image_frame"><a href="#"><img src="../images/autocad-lt-2017-elevations.png" alt="" /></a></div>         
   </div>
</div>
<iframe width="560" height="315" src="https://www.youtube.com/embed/c1kGuiYEHh0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

  <div class="clearfix divider_line2"></div>
  <div class="one_half">
    
     <h2> Why @ Hi-Will…?</h2>
  <ul class="fullimage_box">
      <ul class="list5">
        <li>Experienced Faculties</li>
        <li>Audio Visual Training Methodology</li>
        <li>Has Own Machine Shop</li>
        <li>Closely working with industries</li>
        <li>Skill based training</li>
        <li>Placement assistance</li>
        <li>After placement support</li>
        <li>Free up gradation training</li>
        <li>Money back guarantee</li>
      </ul>
  </ul>
  </div>

   <div class="one_half last">
    
      <h2> Who we are…?</h2>
  <ul class="fullimage_box">
      <ul class="list5">
        <li>Hi-WiLL Engineering Solution</li> 
        <li>Hi-WiLL Education</li>    
        <li>Hi-TEC Auto parts</li>    
        <li>Sprint mat</li>     
        <li>Tru-Turn Metal machining</li> 
        <li>Impel Motors</li> 
        <li>Impel Transmission</li>
        <li>Impetus Prolific Pvt.Ltd.</li>
      
      </ul>
  </ul>
  </div>

 
</div>
<?php include "../includes/right-sidebar.php" ?>
</div>
<div class="mar_top4"></div>
<?php include "../includes/footer.php" ?>
</div>
</div>
<?php include "../includes/ga.php" ?>
<?php include "../includes/js.php" ?>
</body>
</html>
