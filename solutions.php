<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>Solutions - Genesis Telesecure LLp</title>
	
	<meta charset="utf-8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.ico" />
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css' />
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    
<!-- just remove the below comments witch color skin you want to use -->
    <!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/lightgreen.css" />-->
    <link rel="stylesheet" href="css/colors/blue.css" />
    <!--<link rel="stylesheet" href="css/colors/green.css" />-->
    <!--<link rel="stylesheet" href="css/colors/red.css" />-->
    <!--<link rel="stylesheet" href="css/colors/cyan.css" />-->
    <!--<link rel="stylesheet" href="css/colors/purple.css" />-->
    <!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
    <!--<link rel="stylesheet" href="css/colors/brown.css" />-->
    
<!-- just remove the below comments witch bg patterns you want to use --> 
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-default.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-one.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-two.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-three.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-four.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-five.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-six.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-seven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eight.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-nine.css" />-->
    
   
    
    <!-- REVOLUTION SLIDER -->
    <link rel="stylesheet" type="text/css" href="js/revolutionslider/css/fullwidth.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="js/revolutionslider/rs-plugin/css/settings.css" media="screen" />
    
    <!-- jquery jcarousel -->
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
	
    <!-- thumbs zoom -->
    <link rel="stylesheet" href="js/thumbszoom/thumbzoom.css" type="text/css" media="all" />
 	
    <!-- faqs -->
    <link rel="stylesheet" href="js/accordion/accordion.css" type="text/css" media="all" />
    
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>


    
<div class="site_wrapper">
	
    <div class="container_full">
    

    	
     <?php include "/includes/header.php" ?>

</div><!-- end top -->

<div class="clearfix"></div>
 
<!-- Slider
======================================= -->  

<div class="container_full">
    
    <div class="fullwidthbanner-container">
    
		<div class="fullwidthbanner">
        
						<ul>
							
                            <!-- SLIDE 3 -->
							<li data-transition="fade" data-slotamount="6" data-thumb="images/sliders/revolution/slider-bg3.jpg">
								
                                <img src="images/sliders/revolution/slider-bg3.jpg" alt="" />
                                
                                <div class="caption lft" data-x="545" data-y="100" data-speed="900" data-start="700" data-easing="easeOutBack"><img src="images/sliders/revolution/slider-mon-img.png" alt="" /></div>
                              

                                <div class="caption lft big_white" data-x="10" data-y="100" data-speed="900" data-start="700" data-easing="easeOutExpo">Desktop Tablet &amp; Mobiles</div>
								<div class="caption lft large_text_two" data-x="10" data-y="149" data-speed="900" data-start="1300" data-easing="easeOutExpo">Responsive Theme</div>
								<div class="caption lfb medium_grey" data-x="10" data-y="220" data-speed="900" data-start="1900" data-easing="easeOutExpo">Benefits of Using Anova</div>
                                
                                <div class="caption lfb small_text" data-x="10" data-y="256" data-speed="900" data-start="2500" data-easing="easeOutExpo">It comes with Unique Pages and Awesome Slideshows,<br />Unique Color Variations. Easy-to-customize and fully<br /> featured design Theme suitable for Company, Business,<br /> Blog &amp; Portfolio Create Outstanding Website in Minutes!</div>
                                
                                <div class="caption lfb small_text" data-x="10" data-y="400" data-speed="900" data-start="3100" data-easing="easeOutExpo"><a href="#" class="button_slider_02">get started now!</a></div>
                               
							</li>
                            
							
                          
                            	
						</ul>
					</div>
				</div>


</div><!-- end slider -->
<div class="clearfix multy_hlines"></div>
<div class="container">
<h2><i>our</i> clients/partners</h2>
  <div class="get_features">

            <div class="one_third">
            
                <ul class="get_features_list">
                    <li class="left"><img src="images/site-icon5.png" alt="" /></li>
                    <li class="right">
                    <h5>CCTV & Survellaince</h5>
                    <p>Lorem lpsum simply dummy typesetting industry ever.</p></li>
                </ul>
                
                <ul class="get_features_list">
                    <li class="left"><img src="images/site-icon7.png" alt="" /></li>
                    <li class="right">
                    <h5>Board Room</h5>
                    <p>Lorem lpsum simply dummy typesetting industry ever.</p></li>
                </ul>
                
                <ul class="get_features_list">
                    <li class="left"><img src="images/site-icon9.png" alt="" /></li>
                    <li class="right">
                    <h5>Telecom</h5>
                    <p>Lorem lpsum simply dummy typesetting industry ever.</p></li>
                </ul>
                
            </div>
            
            <div class="one_third">
            
                <ul class="get_features_list">
                    <li class="left"><img src="images/site-icon11.png" alt="" /></li>
                    <li class="right">
                    <h5>Fire Alarm</h5>
                    <p>Lorem lpsum simply dummy typesetting industry ever.</p></li>
                </ul>
                
                <ul class="get_features_list">
                    <li class="left"><img src="images/site-icon8.png" alt="" /></li>
                    <li class="right">
                    <h5>Office Automation</h5>
                    <p>Lorem lpsum simply dummy typesetting industry ever.</p></li>
                </ul>
                
                <ul class="get_features_list">
                    <li class="left"><img src="images/site-icon10.png" alt="" /></li>
                    <li class="right">
                    <h5>Fullwidth &amp; Boxed</h5>
                    <p>Lorem lpsum simply dummy typesetting industry ever.</p></li>
                </ul>

            </div>
            
            <div class="one_third last">
            
                <ul class="get_features_list">
                    <li class="left"><img src="images/site-icon6.png" alt="" /></li>
                    <li class="right">
                    <h5>Access Control</h5>
                    <p>Lorem lpsum simply dummy typesetting industry ever.</p></li>
                </ul>
                
                <ul class="get_features_list">
                    <li class="left"><img src="images/site-icon8.png" alt="" /></li>
                    <li class="right">
                    <h5>Networking</h5>
                    <p>Lorem lpsum simply dummy typesetting industry ever.</p></li>
                </ul>
              
                <ul class="get_features_list last">
                    <li class="left"><img src="images/site-icon12.png" alt="" /></li>
                    <li class="right">
                    <h5>Video Conference</h5>
                    <p>Lorem lpsum simply dummy typesetting industry ever.</p></li>
                </ul>
                
            </div>
 
        </div>

</div>


<div class="clearfix mar_top4"></div>

<div class="bottom_hilight">

	<div class="container">

    	<div class="left">
        <iframe src="http://www.youtube.com/embed/Iyzxm3-5bRU?wmode=opaque"></iframe>
        </div>
        
        <div class="right">
        	<strong>This section can be use for video or imp services or hilighted thing</strong>
            <p>All the Lorem Ipsum generators on the Internetis tend to repeat predefined chunks as necessary you need to be sure there isn't anything embarrassing hidden in the middle making therefore always free this the first true generator on the Internet.</p>
            <a href="#" class="trans_button">get more info</a>
        </div>
    	
    </div>
	
</div>

<div class="fullgray_area">
<div class="container">

	<div class="one_half">
    
    	<div class="accrodation">
        
    	<h2>FAQS</h2>
        
        <!-- section 1 -->
        <span class="acc-trigger active"><a href="#">Responsive HTML5 &amp; CSS3 Theme</a></span>
        <div class="acc-container">
        <div class="content">
        <strong>There are many variations of passages of Lorem Ipsum available but the majority have suffered alteration in some form, by injected humour, or randomised words which looks desktop publishing packages and web page editors.</strong>
        </div>
        </div>
        
        <!-- section 2 -->
        <span class="acc-trigger"><a href="#">Cross Browser Support &amp; Layered PSDs</a></span>
        <div class="acc-container">
        <div class="content">
            <strong>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration.</strong>
        </div>
        </div>
        
        <!-- section 3 -->
        <span class="acc-trigger"><a href="#">Slideshows, Colors, Images &amp; Shortcodes</a></span>
        <div class="acc-container">
        <div class="content">
            <strong>There are many variations of but the majority have suffered alteration in some form, by injected humour, or randomised words which looks.</strong>
            </div>
        </div>
        
    	</div>
        
	</div><!-- end resions to you choose us -->
    
    <div class="one_half last">
    
        <h2><i>Client</i> says</h2>
        
        <div class="testimonials-9">
        
        	<div class="contarea">
            There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks
            </div>
            <div class="downarrow"></div>
            <div class="client_img">
            <img src="images/site-img29.png" alt="" />
            <strong>- Lucia Florenceancy</strong>
            <br />&nbsp;customer
            </div>
    	
        </div>
        
    </div><!-- end client says -->

</div>
    </div>

<div class="clearfix multy_hlines"></div>

<?php include "/includes/partner.php" ?>

        <div class="clearfix mar_top4"></div>





<!-- Footer
======================================= -->

<?php include "/includes/footer.php" ?>


<!-- end style switcher -->
 
</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery from the google apis -->
<script type="text/javascript" src="js/universal/jquery.js"></script>

<!-- thumbs zoom -->
<script src="js/thumbszoom/jquery-1.3.1.min.js"></script>
<script src="js/thumbszoom/custom.js"></script>

<!-- style switcher -->
<script src="js/style-switcher/jquery-1.js"></script>
<script src="js/style-switcher/styleselector.js"></script>

<!-- main menu -->
<script type="text/javascript" src="js/mainmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/mainmenu/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/mainmenu/selectnav.js"></script>

<!-- jquery jcarousel -->
<script type="text/javascript" src="js/jcarousel/jquery.jcarousel.min.js"></script>

<!-- REVOLUTION SLIDER -->
<script type="text/javascript" src="js/revolutionslider/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="js/mainmenu/scripts.js"></script>

<!-- top show hide plugin script-->
<script src="js/show-hide-plugin/showHide.js" type="text/javascript"></script>

<!-- scroll up -->
<script type="text/javascript">
    $(document).ready(function(){
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
 
    });
</script>

<!-- jquery jcarousel -->
<script type="text/javascript">

	jQuery(document).ready(function() {
			jQuery('#mycarousel').jcarousel();
	});
	
	jQuery(document).ready(function() {
			jQuery('#mycarouseltwo').jcarousel();
	});
	
	jQuery(document).ready(function() {
			jQuery('#mycarouselthree').jcarousel();
	});
	
	jQuery(document).ready(function() {
			jQuery('#mycarouselfour').jcarousel();
	});
	
</script>

<!-- accordion -->
<script type="text/javascript" src="js/accordion/custom.js"></script>

<!-- REVOLUTION SLIDER -->
<script type="text/javascript">

		var tpj=jQuery;
		tpj.noConflict();

		tpj(document).ready(function() {

		if (tpj.fn.cssOriginal!=undefined)
			tpj.fn.css = tpj.fn.cssOriginal;

			tpj('.fullwidthbanner').revolution(
				{
					delay:9000,
					startwidth:1000,
					startheight:560,

					onHoverStop:"on",						// Stop Banner Timet at Hover on Slide on/off

					thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
					thumbHeight:50,
					thumbAmount:3,

					hideThumbs:200,
					navigationType:"none",				// bullet, thumb, none
					navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none

					navigationStyle:"round",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


					navigationHAlign:"right",				// Vertical Align top,center,bottom
					navigationVAlign:"bottom",					// Horizontal Align left,center,right
					navigationHOffset:50,
					navigationVOffset:55,

					soloArrowLeftHalign:"left",
					soloArrowLeftValign:"center",
					soloArrowLeftHOffset:0,
					soloArrowLeftVOffset:0,

					soloArrowRightHalign:"right",
					soloArrowRightValign:"center",
					soloArrowRightHOffset:0,
					soloArrowRightVOffset:0,

					touchenabled:"on",						// Enable Swipe Function : on/off



					stopAtSlide:-1,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
					stopAfterLoops:0,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic



					fullWidth:"on",

					shadow:0								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

				});




	});
	</script>



</body>
</html>
