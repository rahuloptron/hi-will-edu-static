<!doctype html>
<html lang="en-gb" class="no-js"> 
<head>
	<title>Hi-Will Education</title>
	<meta charset="utf-8" />
	<meta name="keywords" content="" />
	<meta name="description" content="No.1 CCTV Dealer in mumbai, Fire Alarm, Time Attendance, IP Camera, Networking Solutions with 20 years of experience with 500+ Projects and 700+ Customers." />
	<link rel="shortcut icon" href="images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css' />	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />	
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
   
  <!-- sticky menu -->
    <link rel="stylesheet" href="js/sticky-menu/core.css">

</head>
<body>
<div class="site_wrapper">
<div class="container_full">   	
<?php include "includes/header.php" ?>
</div>
<div class="clearfix"></div>
  <div class="container_full">
    	<div class="slider_static_image">
      	<div class="container">
             	<div class="content_fullwidth">
      <div class="one_full">  
    
<div class="big_text2">Welcome to Hi-WiLL Education
</div>
<p>To Shape the future of the Society by
discovering new technologies and share
our resources with the world.</p>

    </div>
    <p>10+ years | 500+ Students | 20+ Trainers
    </p>
    <div class="right"><a href="/contact.html" class="download_profile">Enroll Now</a></div>
           </div>         
    </div>	
   </div>    
</div>
 <div class="clearfix"></div>
 <div class="fullcustom_area">
 <div class="container">
	<div class="content_fullwidth2">
    	<div class="one_third">
        	<div class="portfolio_image">
            <a class="fancybox" href="/courses/certificate-course-in-cad-cam-unigraphics-nx.html" data-fancybox-group="gallery" title="Telecom courses">
            <img src="images/courses-image1.jpg" alt="" />
            <div class="title">Certificate Course in CAD/CAM with Unigraphics NX</div>
            <div class="subtitle">Duration: 05 Month</div>
            <div class="subtitle">Fees: 24,000/- (<em>10% Discount</em> )</div></a>
            </div>
        </div><!-- end section -->       
        <div class="one_third">
        	<div class="portfolio_image">
            <a class="fancybox" href="/courses/certificate-course-cad-unigraphics-nx-plastic-mold-design.html" data-fancybox-group="gallery" title="CCTV & Survellaince">
            <img src="images/courses-image2.jpg" alt="" />
            <div class="title"> Certificate Course in CAD with Unigraphics NX &amp;Plastic Mold Design</div>
            <div class="subtitle">Duration: 03 Month</div>
            <div class="subtitle">Fees: 14,000/- (<em>10% Discount</em> )</div></a>
            </div>
        </div><!-- end section -->  
        <div class="one_third last">
        	<div class="portfolio_image">
            <a class="fancybox" href="/courses/certificate-course-cam-unigraphics-nx.html" data-fancybox-group="gallery" title="Fire Alarm">
            <img src="images/engineer-courses.jpg" alt="" />
            <div class="title">Certificate Course in CAM with Unigraphics NX</div>
            <div class="subtitle">Duration: 1.5 Month</div>
            <div class="subtitle">Fees: 10,000/- (<em>10% Discount</em> )</div></a>
            </div> 
        </div><!-- end section -->   
        <div class="clearfix mar_top4"></div>
   <div class="one_third">
        	<div class="portfolio_image">
            <a class="fancybox" href="/courses/certificate-course-cam-delcam-powermill.html" data-fancybox-group="gallery" title="Audio & Video">
            <img src="images/courses-image5.jpg" alt="" />
            <div class="title">Certificate Course in CAM with Delcam Powermill</div>
            <div class="subtitle">Duration: 1.5 Month</div>
            <div class="subtitle">Fees: 9,000/- (<em>11% Discount</em> )</div></a>
            </div>
        </div><!-- end section -->
        <div class="one_third">
        	<div class="portfolio_image">
            <a class="fancybox" href="/courses/autocad-lt-2017.html" data-fancybox-group="gallery" title="Access Control">
            <img src="images/courses-image3.jpg" alt="" />
            <div class="title">AutoCAD LT 2017</div>
            <div class="subtitle">Duration: 1 Month</div>
            <div class="subtitle">Fees: 5,000/- (<em>8% Discount</em> )</div></a>
            </div>
        </div><!-- end section -->
        <div class="one_third last">
        	<div class="portfolio_image">
            <a class="fancybox" href="/courses/cnc-operating-and-programming.html" data-fancybox-group="gallery" title="Networking">
            <img src="images/courses-image4.jpg" alt="" />
            <div class="title">CNC Operating and Programming</div>
            <div class="subtitle">Duration: 1 Month</div>
            <div class="subtitle">Fees: 5,000/- (<em>8% Discount</em> )</div></a>
            </div> 
        </div> <!-- end section -->
	</div>
  </div>
</div>
<div class="fullgray_area">
<div class="container">
<div class="two_third">
      <h2>What we do?</h2> 
      <div class="fullimage_box">
            <ul class="list5">
        <li>Doing need assessment for manpower in various industries.</li>
        <li>Performing skill gap analysis for various sectors.</li>
        <li>Preparing training program based on these analysis.</li>
        <li>Training to students, engineers and employees of company, industrialists, etc.</li>
        <li>All courses are regularly updates and changes are made as per industrial scenario.</li>
        <li>Currently we are doing skill base training on various subjects like CAD, CAM, Die Designing and CNC Programming.</li>
        <li>The courses are designed as per the industrial requirement.</li>
        <li>Placement of the trainees and after training support.</li>
   
            </ul>      
      </div>

        </div><!-- end about site area -->
      <div class="one_third last"> 
      <h2>What do we offer?</h2> 
     <div class="fullimage_box">
            <ul class="list5">
				<li>Unigraphics NX CAD</li> 
        <li>1Unigraphics NX CAM</li>    
        <li>Delcam Powermill</li>    
        <li>Solid Edge CAD</li>     
        <li>AutoCAD for Mechanical and Civil</li> 
        <li>CNC Programming</li> 
        <li>Die Designing</li>
        <li>Consultancy</li>
        <li>Industrial training on sales, quality and productivity.</li>
       			</ul>      
    	</div>      
    </div>	
</div>
</div>
<div class="fullcustom2_area">
<div class="container">
<div class="one_half">
	  <h2>Student Speaks</h2> 
        <div class="testimonials-9">
        	<div class="contarea">
         "The Hi-Will Education Academy has helped to enhance my technical abilities by providing substantial, in-depth knowledge as well as direct interaction at real project sites. The training was inspirational, energising and brought in a lot of ideas. It also has high pedagogical standards and its own individualism in developing quality skills within a person. Post the training, I am now working with the L&T Solar department as a Commission & Service Engineer at a Solar Projects Site." - Shubham Anand
            </div>
            <div class="downarrow"></div>
            <div class="client_img">
              <strong> Mr. abc</strong>
            <br />&nbsp;Engineer

            </div>
              </div>
	</div><!-- end resions to you choose us -->
     <div class="one_half last">
  <h2>Student Speaks</h2>
  <div class="testimonials-9">
	<div class="contarea">
        "The Hi-Will Education Academy has helped to enhance my technical abilities by providing substantial, in-depth knowledge as well as direct interaction at real project sites. The training was inspirational, energising and brought in a lot of ideas. It also has high pedagogical standards and its own individualism in developing quality skills within a person. Post the training, I am now working with the L&T Solar department as a Commission & Service Engineer at a Solar Projects Site."
            </div>
            <div class="downarrow"></div>
            <div class="client_img">
            <strong> Mr. xyz</strong>
            <br />&nbsp;Engineer
            </div>
        </div>
    </div><!-- end client says -->
</div>
</div>
<div class="clearfix mar_bottom4"></div>  

<div class="punchline_text_home">
	<div class="container">
   	<h2>Right Solutions at Right Price
		</h2>
       <strong><a href="/contact.html" class="morebut">Enroll Now</a></strong>
    </div>
</div>
<?php include "includes/footer.php" ?>
</div>
<?php include "includes/js.php" ?>


</body>
</html>
