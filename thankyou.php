<!doctype html>
 <html lang="en-gb" class="no-js"> 
 <head>
	<title>Thank You - Genesis Telesecure LLP</title>
	<meta charset="utf-8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link rel="shortcut icon" href="images/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css' />	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    <link rel="stylesheet" href="css/colors/blue.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<div class="site_wrapper">
 <div class="container_full">
 <?php include "includes/header.php" ?>
</div><!-- end top -->
<div class="clearfix"></div>
<div class="page_title">
<div class="container">
		<div class="title"><h1>Thank You</h1></div>
</div>
</div>
<div class="clearfix"></div>   
<div class="container">
	<div class="content_fullwidth">        	
    <div class="one_half">
           <div class="error_pagenotfound">
    	<h1> Thank You!</h1>
        
<h2>Check your Email Inbox </h2>
<div class="image_frame"><a href="#"><img src="images/envelope-icon.jpg" alt="" /></a></div>
 <p>Your submission is received and we will contact you soon</p>
 
        <div class="clearfix margin_top3"></div>
    	
        <a href="index.html" class="but_medium1"><i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp; Go to Back</a>
        </div>
<div class="clearfix"></div> 
</div>               
    <div class="one_half last">
	  <h2>Customer Speaks</h2> 
        <div class="testimonials-9">
        	<div class="contarea">
          We call genesis our technology partner, they installed CCTV, Time Attendance system & EPABX in our Mumbai HO and Nashik factory. 
          They are expert in their field and we always recommend them to our partners for anything related to office automation.  
            </div>
            <div class="downarrow"></div>
            <div class="client_img">
              <strong> Mr. Waman</strong>
            <br />&nbsp;Adam Fabriwerk Pvt. Ltd.

            </div>
              </div>
	</div>
</div>
</div><!-- end main content area -->
<div class="clearfix"></div>
<div class="mar_top4"></div>
<div class="clearfix bottom_buildings"></div><!-- end buildings graph -->
</div>
<?php include "includes/footer.php" ?>

<link rel="alternate stylesheet" type="text/css" href="css/colors/blue.css" title="blue" />
</div>
 <?php include "includes/js.php" ?>
<?php include "includes/ga.php" ?>
</body>
</html>
