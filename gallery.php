<!doctype html>
<html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
	<title>Gallery - Hi-Will Education</title>
	
	<meta charset="utf-8" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="images/favicon.ico" />
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css' />
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/style.css" type="text/css" />
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    

    <!--<link rel="stylesheet" href="css/colors/blue.css" />-->
   
    
    <!-- style switcher -->
    <link rel="stylesheet" media="screen" href="js/style-switcher/color-switcher.css" />

    <!-- jquery jcarousel -->
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin.css" />
    <link rel="stylesheet" type="text/css" href="js/jcarousel/skin2.css" />
 
	<!-- fancyBox -->
    <link rel="stylesheet" type="text/css" href="js/portfolio/source/jquery.fancybox.css" media="screen" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body>


<div class="site_wrapper">
	
    <div class="container_full">
    
        <?php include "includes/header.php" ?>

</div><!-- end top -->

<div class="clearfix"></div>
 
<div class="page_title">

	<div class="container">
		<div class="title"><h1>Gallery</h1></div>
       <!-- <div class="pagenation"><a href="index.html">Home</a> <i>/</i> <a href="#">Portfolio</a> <i>/</i> 4 Columns</div>-->
	</div>
</div><!-- end page title --> 

<div class="clearfix"></div>   
 

<!-- Contant
======================================= -->

<div class="container">

<div class="content_fullwidth">
    
    <h2>Images of <strong>Team</strong></h2>
        
    <div class="one_third">
          <div class="portfolio_image">
            <i class="fa fa-search fa-4x"></i>
            <a class="fancybox" href="images/gallery/26.jpg" data-fancybox-group="gallery" title="Publishing has packages"><img src="images/gallery/26.jpg" alt=""></a>
            
            </div>
        </div><!-- end section -->
        
        <div class="one_third">
          <div class="portfolio_image">
            <i class="fa fa-search fa-4x"></i>
            <a class="fancybox" href="images/gallery/24.jpg" data-fancybox-group="gallery" title="Desktop publish packages"><img src="images/gallery/24.jpg" alt=""></a>
            
            </div>
        </div><!-- end section -->
        
        <div class="one_third last">
          <div class="portfolio_image">
            <i class="fa fa-search fa-4x"></i>
            <a class="fancybox" href="images/gallery/22.jpg" data-fancybox-group="gallery" title="Prity have suffered alteration form"><img src="images/gallery/22.jpg" alt=""></a>
            
            </div> 
        </div><!-- end section -->
        
        <div class="clearfix mar_top5"></div>

        <h2>Images of <strong>Training</strong></h2>
        
        <div class="one_third">
          <div class="portfolio_image">
            <a class="fancybox-media" href="images/gallery/14.jpg" title="Majority have alteration"><img src="images/gallery/14.jpg" alt=""></a>
            
            </div>
        </div><!-- end section -->
        
        <div class="one_third">
          <div class="portfolio_image">
            <a class="fancybox-media" href="images/gallery/13.jpg" title="Available but the majority"><img src="images/gallery/13.jpg" alt=""></a>
            
            </div>
        </div><!-- end section -->
        
        <div class="one_third last">
          <div class="portfolio_image">
            <i class="fa fa-search fa-4x"></i>
            <a class="fancybox" href="images/gallery/15.jpg" data-fancybox-group="gallery" title="Slteration suffer ation"><img src="images/gallery/15.jpg" alt=""></a>
            
            </div> 
        </div><!-- end section -->
    
         <div class="clearfix mar_top5"></div>
        
        <div class="one_third">
          <div class="portfolio_image">
            <a class="fancybox-media" href="images/gallery/17.jpg" title="Majority have alteration"><img src="images/gallery/17.jpg" alt=""></a>
            
            </div>
        </div><!-- end section -->
        
        <div class="one_third">
          <div class="portfolio_image">
            <a class="fancybox-media" href="images/gallery/18.jpg" title="Available but the majority"><img src="images/gallery/18.jpg" alt=""></a>
            
            </div>
        </div><!-- end section -->
        
        <div class="one_third last">
          <div class="portfolio_image">
            <i class="fa fa-search fa-4x"></i>
            <a class="fancybox" href="images/gallery/16.jpg" data-fancybox-group="gallery" title="Slteration suffer ation"><img src="images/gallery/16.jpg" alt=""></a>
            
            </div> 
        </div><!-- end section -->

         <div class="clearfix mar_top5"></div>
  </div>

</div>
<div id="footer">
<div class="footer_columns">

<div class="copyright_info">
<div class="container">
<div class="three_fourth">
<div id="powered"><a href="index.html" target="_blank">Genesis Telesecure LLP </a> © 2017 | Designed and Promoted by
<a href="http://www.optronmarketing.com">Optron</a> 
</div> 
</div>
<div class="one_fourth last">
	<a href="/privacy-policy.html">Privacy Policy</a> | <a href="/terms-of-use.html">Terms and conditions</a> 
</div>
</div>
</div>
</div>
<?php include "includes/js.php" ?>
<?php include "includes/ga.php" ?>
</body>
</html>
